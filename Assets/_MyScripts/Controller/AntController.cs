using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class AntController : MonoBehaviour
{
    // Random Value
    private float randomVariable;
    
    // Settings
    public float MoveSpeed = 5;
    public float BodySpeed = 5;
    public float SteerSpeed = 180;
    public int Gap = 10;

    // References
    public GameObject BodyPrefab;
    public GameObject OrangePrefab;
    public GameObject GrapePrefab;
    public GameObject TomatoPrefab;
    public GameObject PumpkinPrefab;
    public GameObject Donut1Prefab;
    public GameObject Donut2Prefab;
    public GameObject Donut3Prefab;
    public GameObject BombPrefab;
    
    // Lists
    private List<GameObject> BodyParts = new List<GameObject>();
    private List<Vector3> PositionsHistory = new List<Vector3>();
    
    public List<GameObject> GetBodyParts()
    {
        return BodyParts;
    }
    

    // Update is called once per frame
    void Update() {

        // Move forward
        transform.position += transform.forward * MoveSpeed * Time.deltaTime;

        // Steer
        float steerDirection = Input.GetAxis("Horizontal"); // Returns value -1, 0, or 1
        transform.Rotate(Vector3.up * steerDirection * SteerSpeed * Time.deltaTime);

        // Store position history
        PositionsHistory.Insert(0, transform.position);

        // Move body parts
        int index = 0;
        foreach (var body in BodyParts) 
        {
            Vector3 point = PositionsHistory[Mathf.Clamp(index * Gap, 0, PositionsHistory.Count - 1)];

            // Move body towards the point along the ants path
            Vector3 moveDirection = point - body.transform.position;
            body.transform.position += moveDirection * BodySpeed * Time.deltaTime;

            // Rotate body towards the point along the ants path
            body.transform.LookAt(point);
            index++;
        }
    }

    private void GrowAnt()
    {
        // Instantiate body instance
        GameObject body = Instantiate(BodyPrefab);

        // Add it to the list
        BodyParts.Add(body);

        // Set the position of the new body part
        if (BodyParts.Count > 1)
        {
            // If it's not the first body part, position it close to the previous body part
            Vector3 offset = transform.forward * -2.0f; // Adjust this value based on your preference
            body.transform.position = BodyParts[BodyParts.Count - 2].transform.position + offset;
        }
        else
        {
            // If it's the first body part, position it at a greater distance from the ant
            Vector3 antToFirstPart = transform.forward * -15.0f; // Adjust this value based on your preference
            body.transform.position = transform.position + antToFirstPart;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            Debug.Log("Collided with a wall!");
            Destroy(gameObject);
            
            // Small ant randomly walk away
            foreach (var body in BodyParts)
            {
                Vector3 randomDirection = new Vector3(Random.Range(-2.0f, 2.0f), 0.0f, Random.Range(-2.0f, 2.0f)).normalized;
                body.transform.Translate(randomDirection * 2.0f, Space.World);
            }
            
            // Remove the CameraFollow component from the MainCamera
            CameraFollow cameraFollow = Camera.main.GetComponent<CameraFollow>();
            if (cameraFollow != null)
            {
                Destroy(cameraFollow);
            }
        }

        if(other.CompareTag("Food"))
        {
            Debug.Log("Collided with a Food!"); 
            Destroy(other.gameObject);
            GrowAnt();
            
            ScoreManager scoreManager = GetComponent<ScoreManager>();
            if (scoreManager != null)
            {
                // Add score when the ant eats food
                scoreManager.AddScore(10);
            }
            
            // Set the randomVariable to a random value between -10.0f and 10.0f
            randomVariable = Random.Range(0.0f, 10.0f);
            
            // Output the random value to the console
            Debug.Log("Random Variable: " + randomVariable);
            
            //Food
            if (randomVariable >= 0 && randomVariable < 1.5)
            {
                Instantiate(OrangePrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
                Instantiate(GrapePrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
            }
            else if (randomVariable >= 1.5 && randomVariable < 3)
            {
                Instantiate(OrangePrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
                Instantiate(TomatoPrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
            }
            else if (randomVariable >= 3 && randomVariable < 4.5)
            {
                Instantiate(TomatoPrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
                Instantiate(GrapePrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
            }
            //Donut
            else if (randomVariable >= 4.5 && randomVariable < 5.5)
            {
                Instantiate(Donut1Prefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
            }
            else if (randomVariable >= 5.5 && randomVariable < 6.5)
            {
                Instantiate(Donut2Prefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
            }
            else if (randomVariable >= 6.5 && randomVariable <= 7.5)
            {
                Instantiate(Donut3Prefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
            }
            //Bomb
            else if (randomVariable >= 7.5 && randomVariable <= 9)
            {
                Instantiate(BombPrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
                Instantiate(GrapePrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
            }
            //Pumpkin
            else if (randomVariable >= 9 && randomVariable <= 10)
            {
                Instantiate(PumpkinPrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
            }
        }
        
        if (other.CompareTag("Bomb"))
        {
            Debug.Log("Collided with a Bomb!"); 
            Destroy(other.gameObject);
            
            ScoreManager scoreManager = GetComponent<ScoreManager>();
            if (scoreManager != null)
            {
                // Add score when the ant eats food
                scoreManager.AddScore(-50);
            }
        }
        
        if(other.CompareTag("Donut"))
        {
            Debug.Log("Collided with a Donut!"); 
            Destroy(other.gameObject);
            GrowAnt();
            
            ScoreManager scoreManager = GetComponent<ScoreManager>();
            if (scoreManager != null)
            {
                // Add score when the ant eats food
                scoreManager.AddScore(50);
            }
            
            Instantiate(TomatoPrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
        }
        
        if(other.CompareTag("Pumpkin"))
        {
            Debug.Log("Collided with a Pumpkin!"); 
            Destroy(other.gameObject);
            GrowAnt();
            
            ScoreManager scoreManager = GetComponent<ScoreManager>();
            if (scoreManager != null)
            {
                // Add score when the ant eats food
                scoreManager.AddScore(100);
            }
            
            Instantiate(OrangePrefab, new Vector3(Random.Range(-10.0f, 10.0f), 0.0f, Random.Range(-10.0f, 10.0f)), Quaternion.identity);
        }
    }
}
