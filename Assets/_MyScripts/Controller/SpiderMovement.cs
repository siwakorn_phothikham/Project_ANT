using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderMovement : MonoBehaviour
{
    public float speed = 5f; // Adjust the speed as needed
    public float moveDistance = 10f; // Adjust the loop distance as needed
    public bool MoveLeftRight;
    public bool MoveUpDown;
    
    private bool movingRight = true;
    private bool movingUp = true;
    private float initialPositionLeftRight;
    private float initialPositionUpDown;
    
    
    void Start()
    {
        initialPositionLeftRight = transform.position.x;
        initialPositionUpDown = transform.position.z;
    }

    void Update()
    {
        if (MoveLeftRight == true && MoveUpDown == false)
        {
            if (movingRight)
            {
                transform.Translate(Vector3.right * speed * Time.deltaTime);

                if (transform.position.x >= initialPositionLeftRight + moveDistance)
                {
                    movingRight = false;
                }
            }
            else
            {
                transform.Translate(Vector3.left * speed * Time.deltaTime);

                if (transform.position.x <= initialPositionLeftRight)
                {
                    movingRight = true;
                }
            }
        }
        else if (MoveLeftRight == false && MoveUpDown == true)
        {
            if (movingUp)
            {
                transform.Translate(Vector3.forward * speed * Time.deltaTime); // Move along the positive Z-axis

                if (transform.position.z >= initialPositionUpDown + moveDistance)
                {
                    movingUp = false;
                }
            }
            else
            {
                transform.Translate(Vector3.back * speed * Time.deltaTime); // Move along the negative Z-axis

                if (transform.position.z <= initialPositionUpDown)
                {
                    movingUp = true;
                }
            }
        }
    }
}