using UnityEngine;
using TMPro;

public class HighScoreManager : MonoBehaviour
{
    private const string HighScoreKey = "HighScore";

    public TextMeshProUGUI highScoreText;

    private void Start()
    {
        LoadHighScore();
    }

    public void SaveHighScore(int currentScore)
    {
        int highScore = PlayerPrefs.GetInt(HighScoreKey, 0);

        if (currentScore > highScore)
        {
            highScore = currentScore;
            PlayerPrefs.SetInt(HighScoreKey, highScore);
            PlayerPrefs.Save();
            UpdateHighScoreUI(highScore);
        }
    }

    public void ResetHighScore()
    {
        PlayerPrefs.DeleteKey(HighScoreKey);
        LoadHighScore();
    }

    private void LoadHighScore()
    {
        int highScore = PlayerPrefs.GetInt(HighScoreKey, 0);
        UpdateHighScoreUI(highScore);
    }

    private void UpdateHighScoreUI(int score)
    {
        if (highScoreText != null)
        {
            highScoreText.text = "High Score: " + score;
        }
    }
}