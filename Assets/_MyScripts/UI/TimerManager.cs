using UnityEngine;
using TMPro;
using System.Collections.Generic;

public class TimerManager : MonoBehaviour
{
    public float gameTime = 300.0f;
    private float currentTime;

    public TextMeshProUGUI timerText;

    private bool isGameActive = true;

    private void Start()
    {
        currentTime = gameTime;
        UpdateTimerUI();
    }

    private void Update()
    {
        if (isGameActive)
        {
            currentTime -= Time.deltaTime;

            if (currentTime <= 0)
            {
                OnGameTimeUp();
            }

            UpdateTimerUI();
        }
    }

    private void UpdateTimerUI()
    {
        if (timerText != null)
        {
            timerText.text = "Time: " + Mathf.CeilToInt(currentTime).ToString();
        }
    }

    private void OnGameTimeUp()
    {
        Debug.Log("Game Over - Time Up!");

        // Access the AntController script on the same GameObject
        AntController antController = GetComponent<AntController>();
        if (antController != null)
        {
            // Retrieve the BodyParts list and perform your logic
            List<GameObject> bodyParts = antController.GetBodyParts();

            foreach (var body in bodyParts)
            {
                Vector3 randomDirection = new Vector3(Random.Range(-2.0f, 2.0f), 0.0f, Random.Range(-2.0f, 2.0f)).normalized;
                body.transform.Translate(randomDirection * 2.0f, Space.World);
            }

            // Remove the CameraFollow component from the MainCamera
            CameraFollow cameraFollow = Camera.main.GetComponent<CameraFollow>();
            if (cameraFollow != null)
            {
                Destroy(cameraFollow);
            }
        }

        // Set this to false to stop the game activity
        isGameActive = false;

        // Destroy the TimerManager itself or handle game over actions
        Destroy(gameObject);
    }
}