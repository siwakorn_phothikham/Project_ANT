using UnityEngine;
using UnityEngine.UI;

public class ResetHighScoreButton : MonoBehaviour
{
    public Button resetButton;
    public HighScoreManager highScoreManager;

    private void Start()
    {
        // Attach the button click listener
        resetButton.onClick.AddListener(OnClickResetButton);
    }

    private void OnClickResetButton()
    {
        // Call the ResetHighScore method from the HighScoreManager
        highScoreManager.ResetHighScore();
    }
}