using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public int score = 0;

    //Balanced Score To Change Stage
    public int ScoreStage2 = 0;
    public int ScoreStage3 = 0;
    public int ScoreStage4 = 0;
    public int ScoreStage5 = 0;
    public int ScoreStage6 = 0;
    public int ScoreStage7 = 0;
    public int ScoreStage8 = 0;
    
    //Change Object In Map
    public GameObject Stage2;
    public GameObject Stage3;
    public GameObject Stage4;
    public GameObject Stage5;
    public GameObject Stage6;
    public GameObject Stage7;
    public GameObject Stage8;

    private void Start()
    {
        UpdateScoreUI();
    }

    public void AddScore(int points)
    {
        score += points;
        UpdateScoreUI();
    }
    
    public void MultiplyScore(int points)
    {
        score *= points;
        UpdateScoreUI();
    }
    
    public void DivideScore(int points)
    {
        score = points - points/2;
        UpdateScoreUI();
    }

    private void UpdateScoreUI()
    {
        GetComponent<HighScoreManager>().SaveHighScore(score);

        if (scoreText != null)
        {
            scoreText.text = "Score : " + score;
        }

        if (score >= ScoreStage2)
        {
            Stage2.SetActive(true);
        }
        
        if (score >= ScoreStage3)
        {
            Stage3.SetActive(true);
        }
        
        if (score >= ScoreStage4)
        {
            Stage4.SetActive(true);
        }
        
        if (score >= ScoreStage5)
        {
            Stage5.SetActive(true);
        }
        
        if (score >= ScoreStage6)
        {
            Stage6.SetActive(true);
        }
        
        if (score >= ScoreStage7)
        {
            Stage7.SetActive(true);
        }
        
        if (score >= ScoreStage8)
        {
            Stage8.SetActive(true);
        }
    }
}